#ifndef SED1531_ARDUINO_H
#define SED1531_ARDUINO_H

/*
    This file defines the pin mapping between the GLCD and the Arduino

    This file is part of the SED1531 Arduino library.

    SED1531 Arduino library Copyright (c) 2013 Tom van Zutphen
    http://www.tkkrlab.nl/wiki/Glcd_48x100

    This library is largely based on / ported from from the
    library (c) 2013 by Peter van Merkerk
    http://sourceforge.net/p/glcdsed1531lib/wiki/Home/

    I adapted it for use with Arduino and added some commands to draw circles,
    make it compatible with the arduino print command and add collision detect
    for all drawing functions except text.

    The SED1531 Arduino library is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or (at
    your option) any later version.

    The SED1531 Arduino library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
    Public License for more details.

    You should have received a copy of the GNU General Public License along with
    the SED1531 Arduino library If not, see http://www.gnu.org/licenses/.
*/

/*
    by default this library uses a different pin mapping for the data pins then the other code examples!

    Port and pin mapping for Arduino (ATmega328 @ 16MHz):

    	GLCD
	Pin   signal            	PIN Arduino (ATmega328)

    1     GND					GND
    2     VCC               	+5V (or 3v3 but then backlight will not function)
    3     Backlight	(Act LOW)	GND for on, can be connected to a pin to switch backlight on/off
    4     AO       				Digital 12 	(PB4)
    5     RW  					Digital 11 	(PB3)
    6     E                 	Digital 10 	(PB2)
    7     DB 7              	Digital 7 	(PD7)
    8     DB 6              	Digital 6 	(PD6)
    9     DB 5              	Digital 5 	(PD5)
    10    DB 4              	Digital 4 	(PD4)
    11    DB 3              	Digital 3 	(PD3)
    12    DB 2              	Digital 2 	(PD2)
    13    DB 1              	Digital 9 	(PB1)
    14    DB 0              	Digital 8 	(PB0)

    A different option is to connect DB0...DB7 to Arduino Pin 2...9, this is also used in other code
    Uncomment option 2 below if you prefer this.
*/

#define GLCD_A0  						12		//this can be set to any other free pin
#define GLCD_RW  						11		//this can be set to any other free pin
#define GLCD_ENABLE  					13		//this can be set to any other free pin
#define GLCD_IO_INIT()					pinMode(GLCD_A0, OUTPUT); \
										pinMode(GLCD_RW , OUTPUT); \
										pinMode(GLCD_ENABLE, OUTPUT);\

#define GLCD_IO_PIN_A0_0()             digitalWrite(GLCD_A0, LOW)
#define GLCD_IO_PIN_A0_1()             digitalWrite(GLCD_A0, HIGH)

#define GLCD_IO_PIN_RW_0()             digitalWrite(GLCD_RW, LOW)
#define GLCD_IO_PIN_RW_1()             digitalWrite(GLCD_RW, HIGH)

#define GLCD_IO_PIN_E_0()              digitalWrite(GLCD_ENABLE, LOW)
#define GLCD_IO_PIN_E_1()              digitalWrite(GLCD_ENABLE, HIGH)

#define GLCD_IO_DATA_DIR_OUTPUT()		DDRB |= 0B00000011; DDRD |= 0B11111100
#define GLCD_IO_DATA_DIR_INPUT()		DDRB &= 0B11111100; DDRD &= 0B00000011

/*
 * Option 1:
 * GLCD D0..D1 = Arduino PIN8..PIN9 (Atmega328 PB0..PB1)
 * GLCD D2..D7 = Arduino PIN2..7 (Atmega328 PD2..PD7)
 */
#define SLOW_IO
 
#ifndef SLOW_IO

#define GLCD_IO_DATA_OUTPUT(data)		uint8_t register saveSreg = SREG; \
										cli();                                      \
										PORTB = (PORTB & 0B11111100) | (data&0B11); \
										PORTD = (PORTD & 0B11) | (data&0B11111100); \
										SREG=saveSreg
#define GLCD_IO_DATA_INPUT()           ((PIND & 0B11111100) | (PINB & 0B11))

#else
/*
 * Option 2:
 * GLCD D6..D7 = Arduino PIN8..PIN9 (ATmega328 PB0..PB1)
 * GLCD D0..D5 = Arduino PIN2..7 (ATmega328 PD2..PD7)
 */
#define GLCD_IO_DATA_OUTPUT(data)		uint8_t register saveSreg = SREG; \
										cli();                                      \
										PORTB = (PORTB & 0B11111100) | (data>>6); \
										PORTD = (PORTD & 0B11) | (data<<2); \
										SREG=saveSreg
#define GLCD_IO_DATA_INPUT()           (((PIND & 0B11111100)>>2) | ((PINB & 0B11)<<6))

#endif

#define GLCD_IO_DELAY_READ()           __asm volatile ("nop\nnop\nnop")					//each nop = 62,5ns @16mhz
//#define GLCD_IO_DELAY_WRITE()

#endif // #ifndef SED1531_ARDUINO_H
