#include "SED1531_slow.h"
#include "proportional_font.h"
#include "fixed_font.h"
#include <Wire.h>

SED1531_slow glcd;

#define CMD_NONE  0
#define CMD_CLEAR  1
#define CMD_SET_CURSOR 2
#define CMD_SET_BACKLIGHT 3
#define CMD_SET_FONT 4
#define CMD_IMAGE 5

#define BACKLIGHT_PIN 10

const uint8_t logo[][100] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x7F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x3F,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x70,0xC0,0x00,0x00,0x00,0xC0,0x60,0x00,0x00,0x00,0xC0,0x40,0x40,0x40,0x40,0x00,0x00,0x00,0x80,0xC0,0x40,0x40,0x00,0x00,0x80,0x80,0xC0,0xC0,0x80,0x80,0xC0,0x40,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x40,0xC0,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x1F,0xFF,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0xFF,0x7F,0x71,0x70,0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x07,0x38,0x3C,0x03,0x00,0x00,0x00,0x00,0x3F,0x22,0x22,0x22,0x22,0x00,0x00,0x00,0xFF,0x80,0xC0,0x40,0x40,0x00,0x00,0x00,0x00,0xFF,0x00,0x00,0x00,0x00,0xF8,0x04,0x02,0x01,0x01,0x86,0xCC,0x78,0x00,0x00,0x00,0x00,0xFF,0x08,0x18,0x28,0xCF,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xE0,0x9F,0x80,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x07,0xF8,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x10,0x10,0x10,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x41,0x41,0x41,0x41,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x41,0x40,0x40,0x40,0x40,0x41,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};


void setup(){
  pinMode(BACKLIGHT_PIN, OUTPUT);
  analogWrite(BACKLIGHT_PIN, 0);

  Wire.begin(9);
  Wire.onReceive(receiveWireData);

  glcd.init();
  glcd.setFont(&proportional_font);
  glcd.clearDisplay();
  glcd.setCursor(0,0);
  glcd.print(F("Integratie Token"));

  glcd.setCursor(60, 16);
  glcd.print(F("Vector"));

  showLogo();
}

void loop()
{
  
}

char activeCommand = 0;
char receivedData[18];
uint8_t dataNr = 0;

void showLogo()
{
  for (int r = 0; r < 6; r++)
  {
    glcd.pageUpdateStart(r, 0);
    GLCD_IO_PIN_A0_1();
    for (int i = 0; i < 100; i++)
      glcd.send(logo[r][i]);

    GLCD_IO_PIN_A0_0();
    glcd.pageUpdateEnd();
  }
}

void receiveWireData(int count)
{
  while (Wire.available())
  {
    handleData(Wire.read());
  }
}

void handleData(char data)
{

  switch (activeCommand)
  {
    case CMD_SET_CURSOR:
      receivedData[dataNr++] = data;

      if (dataNr >= 2)
      {
        int row = receivedData[0];
        int col = receivedData[1];

        if (row >= 0 && row <= 48 && col >= 0 && col < 100)
        {
          glcd.setCursor(col, row);
        }
        activeCommand = CMD_NONE;
      }
      break;

    case CMD_SET_BACKLIGHT:
      analogWrite(BACKLIGHT_PIN, (int)data);
      activeCommand = CMD_NONE;
      break;

    case CMD_SET_FONT:
      switch (data) 
      {
        case 1:
         glcd.setFont(&fixed_font);
         break;

        case 2:
          glcd.setFont(&proportional_font);
          break;
      }
      activeCommand = CMD_NONE;
      break;

    case CMD_NONE:
      dataNr = 0;
      if (data < 15)
      {
        switch (data)
        {
          case CMD_CLEAR:
            glcd.clearDisplay();
            analogWrite(BACKLIGHT_PIN, 100);
            break;
  
          case CMD_SET_BACKLIGHT:
          case CMD_SET_CURSOR:
          case CMD_SET_FONT:
            activeCommand = data;
            break;
  
          default:
            break;
        }  
      }
      else 
      {
        glcd.print(data);
      }
      break;

    default:
      activeCommand = CMD_NONE;
      break;
  }
}
